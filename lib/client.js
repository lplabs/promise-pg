var pg = require("pg");
var Q = require("q");

function Client(opts) {
    var client = new pg.Client(opts);
    return wrap(client);
}

exports.Client = Client;
var wrap = exports.wrap = function wrap(client) {
    var wrapped = Object.create(client, {
        raw: {
            value: client
        },
        connect: {
            value: Q.nbind(client.connect, client)
        },
        end: {
            value: Q.nbind(client.end, client)
        },
        query: {
            /**
             * In addition to normal query options, 'opts' can have variable 'buffer' to true or false whether or not to
             * buffer all results.
             * @param opts
             * @param vargs
             * @returns {*} the query object, with an additional 'promise' property that is notified for row elements,
             * and resolved with the result object
             */
            value: function(opts, vargs) {
                var def = Q.defer();
                var query = client.query.apply(client, arguments);

                if(opts.buffer) {
                    query.on("row", function(row, result) {
                        result.addRow(row);
                        def.notify(row);
                    });
                }
                else {
                    query.on("row", function(row, result) {
                        def.notify(row);
                    });
                }


                query.on("end", def.resolve);
                query.on("error", def.reject);
                query.promise = def.promise;

                return query;
            }
        },
        transaction: {
            /**
             * This transaction function receives a callback function that is called after a transaction is started
             * The method should return a promise object that resolves when the actions to be performed in the transaction
             * Are done (which are then committed), or fail if we should rollback
             * @param task
             * @returns {*}
             */
            value: function(task) {
                var ret = wrapped.query("BEGIN").promise
                    .then(task)//Rolling back if we had errors in the transactioned code or waiting on transactioned task.
                    .then(function() { //Completed in transaction code:
                        return wrapped.query("COMMIT").promise;
                    }).catch(function rollback(err) {
                        return wrapped.query("ROLLBACK").promise.thenReject(err); //Rolling back then rejecting with original BEGIN error.
                        //TODO: what if we have an error in rolling back? We now are returning it instead, should we? Which error should we choose?
                    });
                return ret;
            }
        }
    });
    return wrapped;
};