var pg = require("pg");
var pclient = require("./client");
var Q = require("q");

var promisedConnect = Q.nbind(pg.connect, pg);

module.exports = {
    raw: pg, //access defaults etc
    Client: require("./client").Client,
    end: Q.nbind(pg.end, pg),
    connect: function() {
        return promisedConnect.apply(undefined, arguments).then(function(results) {
            results[0] = pclient.wrap(results[0]);
            return results;
        });
    }
};