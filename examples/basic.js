/*
 -- Table: users

 -- DROP TABLE users;

 CREATE TABLE users
 (
 id serial NOT NULL,
 name text NOT NULL,
 t_birth timestamp with time zone,
 country text,
 CONSTRAINT pk_users PRIMARY KEY (id)
 )
 WITH (
 OIDS=FALSE
 );
 ALTER TABLE users
 OWNER TO postgres;
 GRANT ALL ON TABLE users TO example;

 */

var pg = require("../.");
pg.connect("postgres://example:1234@localhost:5432/example").spread(function(client, done) {
    console.log("Connected...");
    var query = client.query({
        text: "select * from users",
        buffer: true //if set to true, adds all the rows to the result object available on the rows property on the promise resolve value
    }); //same query object like pg

    //Rows:
    //Option I:
    query.on("row", function(user) {});
    //Option II:
    query.promise.progress(function(user) {})

    //Done:
    //Option I:
    query.on("end", function(result) { console.log(result.rowCount); });
    //Option II:
    query.promise.done(function(result) { console.log(result.rowCount); });

    //Error:
    //Option I:
    query.on("error", console.log.bind(console));
    //Option II:
    query.promise.catch(console.log.bind(console));

    //Returning:
    //Option I:
    query.on("end", done);
    query.on("error", done);
    //Option II:
    query.promise.finally(done);


    //All together now:


    //Option I:
    query.on("end", function(result) { console.log(result.rowCount); });
    query.on("end", done);
    query.on("row", function(user) {});
    query.on("error", console.log.bind(console));
    query.on("error", done);

    //Option II:
    query.promise.then(
        function(result) { console.log(result); },
        console.log.bind(console),
        function(user) {}
    ).finally(done);
}).done();