var pg = require("../.");
var Q = require("q");
pg.connect("postgres://example:1234@localhost:5432/example").spread(function(client, done) {
    console.log("Connected...");
    var INSERT = "INSERT INTO users(name, t_birth, country) VALUES ($1, $2, $3);";
    
    console.log("Transaction I:")
    var trans = client.transaction(function() {
        //This code runs in a transaction and should return a promise object, that when resolved the transaction is committed
        //But when rejected the transaction is rolled back.

        return Q.all([
            {
                text: INSERT,
                values: ["Jake1", "now()", "Oo"]
            },
            {
                text: INSERT,
                values: ["Cake2", null, "Küche"]
            },
            {
                text: INSERT,
                values: ["Mike3", "now()", null]
            }
        ].map(function(q) {
            return client.query(q).promise;
        }));
    }).then(function() {
            console.log("Good - Committed Transaction I."); //This will happen
        }, function(err) {
            console.log("Bad - Rolled back Transaction I.", err);
        }); //Not returning the connection


    trans.finally(function() {
        client.transaction(function() {
            //This code runs in a transaction and should return a promise object, that when resolved the transaction is committed
            //But when rejected the transaction is rolled back.

            return Q.all([
                {
                    text: INSERT,
                    values: ["Jake", "now()", "Oo"]
                },
                {
                    text: INSERT, //Bad, name is NOT NULL
                    values: [null, null, "Küche"]
                },
                {
                    text: INSERT,
                    values: ["Mike", "now()", null]
                }
            ].map(function(q) {
                return  client.query(q).promise;
            }));
        }).then(function() {
                console.log("Bad - Committed Transaction II."); //Cannot commit due to bad name
            }, function(err) {
                console.log("Good - Rolled back Transaction II.", err.message);
            }).finally(done).done();
    });
}).done();